package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        //VARIABLES
        //Object info = null;
        String [] PIEZAS = null;
        String [] CARRILES = null;
        String [] PINICIO = null;
        String [] COCHES = null;
        String [] CARRERA = null;
        String [] TRAMOS = null;
        
        while ((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                //EN FUNCION DE MI POSICION, ENVIO EL MENSAJE CORRESPONDIENTE (MAXIMO MENSAJES = 1??)
            	
            	//LEO EL MENSAJE Y LO CONVIERTO A STRING
            	Object POS = msgFromServer.data;
            	String s_POS;
            	//System.out.println(POS.toString());
            	
            	//OBTENER EL pieceIndex
            	s_POS = POS.toString();
            	int ini = s_POS.indexOf("pieceIndex")+11, fin = s_POS.indexOf("inPieceDistance")-4;
            	int P_Actual = Integer.parseInt(s_POS.substring(ini, fin));

            	/*
            	//OBTENER EL lane
            	s_POS = POS.toString();
            	ini = s_POS.indexOf("startLaneIndex")+15; fin = s_POS.indexOf("endLaneIndex")-4;
            	int C_Actual = Integer.parseInt(s_POS.substring(ini, fin));
            	*/
            	
            	//OBTENER EL vuelta
            	s_POS = POS.toString();
            	ini = s_POS.indexOf("lap")+4;
            	int V_Actual = Integer.parseInt(s_POS.substring(ini, ini+1));

        		//send(new Turbo("pow pow pow pow pow"));
                //send(new Throttle(0.6));
                //send(new SwitchLane("Right"));
            	
            	switch (V_Actual) {
					case 0:
						switch (P_Actual) {
							case 0:
								send(new Throttle(1.0));
								break;
							case 1:
								send(new Throttle(1.0));
								break;
							case 2:
								send(new Throttle(1.0));
								send(new SwitchLane("Right"));
								break;
							case 3:
								send(new Throttle(0.3));
								break;
							case 4:
								send(new Throttle(0.6));
								break;
							case 5:
								send(new Throttle(0.6));
								break;
							case 6:
								send(new Throttle(0.7));
								break;
							case 7:
								send(new SwitchLane("Left"));
								send(new Throttle(0.7));
								break;
							case 8:
								send(new Throttle(0.9));
								break;
							case 9:
								send(new Throttle(1.0));
								break;
							case 10:
								send(new Throttle(1.0));
								break;
							case 11:
								send(new Throttle(0.6));
								break;
							case 12:
								send(new Throttle(0.6));
								break;
							case 13:
								send(new Throttle(0.4));
								break;
							case 14:
								send(new Throttle(0.4));
								break;
							case 15:
								send(new Throttle(0.6));
								break;
							case 16:
								send(new Throttle(0.7));
								break;
							case 17:
								send(new SwitchLane("Right"));
								send(new Throttle(0.6));
								break;
							case 18:
								send(new Throttle(0.6));
								break;
							case 19:
								send(new Throttle(0.6));
								break;
							case 20:
								send(new Throttle(0.6));
								break;
							case 21:
								send(new Throttle(0.7));
								break;
							case 22:
								send(new Throttle(0.7));
								break;
							case 23:
								send(new Throttle(0.9));
								break;
							case 24:
								send(new Throttle(0.9));
								break;
							case 25:
								send(new Throttle(0.4));
								break;
							case 26:
								send(new Throttle(0.6));
								break;
							case 27:
								send(new Throttle(0.7));
								break;
							case 28:
								send(new SwitchLane("Left"));
								send(new Throttle(0.45));
								break;
							case 29:
								send(new Throttle(0.75));
								break;
							case 30:
								send(new Throttle(0.5));
								break;
							case 31:
								send(new Throttle(0.6));
								break;
							case 32:
								send(new Throttle(0.7));
								break;
							case 33:
								send(new Throttle(0.8));
								break;
							case 34:
								send(new SwitchLane("Right"));
								send(new Throttle(0.9));
								break;
							case 35:
								send(new Throttle(1.0));
								break;
							case 36:
								send(new Throttle(1.0));
								break;
							case 37:
								send(new Throttle(1.0));
								break;
							case 38:
								send(new Throttle(1.0));
								break;
							case 39:
								send(new Throttle(1.0));
								break;
							}
						break;
					case 1:
						switch (P_Actual) {
							case 0:
								send(new Throttle(0.7));
								break;
							case 1:
								send(new Throttle(0.7));
								break;
							case 2:
								send(new Throttle(0.4));
								send(new SwitchLane("Right"));
								break;
							case 3:
								send(new Throttle(0.4));
								break;
							case 4:
								send(new Throttle(0.6));
								break;
							case 5:
								send(new Throttle(0.6));
								break;
							case 6:
								send(new Throttle(0.7));
								break;
							case 7:
								send(new SwitchLane("Left"));
								send(new Throttle(0.4));
								break;
							case 8:
								send(new Turbo("pow pow pow pow pow"));
								send(new Throttle(0.5));
								break;
							case 9:
								send(new Throttle(0.7));
								break;
							case 10:
								send(new Throttle(0.7));
								break;
							case 11:
								send(new Throttle(0.7));
								break;
							case 12:
								send(new Throttle(0.6));
								break;
							case 13:
								send(new Throttle(0.1));
								break;
							case 14:
								send(new Throttle(0.7));
								break;
							case 15:
								send(new Throttle(0.6));
								break;
							case 16:
								send(new Throttle(0.7));
								break;
							case 17:
								send(new SwitchLane("Right"));
								send(new Throttle(0.6));
								break;
							case 18:
								send(new Throttle(0.6));
								break;
							case 19:
								send(new Throttle(0.6));
								break;
							case 20:
								send(new Throttle(0.6));
								break;
							case 21:
								send(new Throttle(0.7));
								break;
							case 22:
								send(new Throttle(0.7));
								break;
							case 23:
								send(new Throttle(0.9));
								break;
							case 24:
								send(new Throttle(0.9));
								break;
							case 25:
								send(new Throttle(0.4));
								break;
							case 26:
								send(new Throttle(0.6));
								break;
							case 27:
								send(new Throttle(0.7));
								break;
							case 28:
								send(new SwitchLane("Left"));
								send(new Throttle(0.45));
								break;
							case 29:
								send(new Throttle(0.75));
								break;
							case 30:
								send(new Throttle(0.5));
								break;
							case 31:
								send(new Throttle(0.6));
								break;
							case 32:
								send(new Throttle(0.7));
								break;
							case 33:
								send(new Throttle(0.8));
								send(new SwitchLane("Right"));
								break;
							case 34:
								send(new Turbo("pow pow pow pow pow"));
								send(new Throttle(0.9));
								break;
							case 35:
								send(new Throttle(1.0));
								break;
							case 36:
								send(new Throttle(1.0));
								break;
							case 37:
								send(new Throttle(1.0));
								break;
							case 38:
								send(new Throttle(1.0));
								break;
							case 39:
								send(new Throttle(1.0));
								break;
						}
					break;
					case 2:
						//tk.beep();
						//System.out.println("Tercera vuelta");
						//send(new Throttle(0.4));
						switch (P_Actual) {
							case 0:
								
								break;
							case 1:
								
								break;
							case 2:
			
								break;
							case 3:
								
								break;
							case 4:
								
								break;
							case 5:
			
								break;
							case 6:
								
								break;
							case 7:
								
								break;
							case 8:
			
								break;
							case 9:
								
								break;
							case 10:
								
								break;
							case 11:
			
								break;
							case 12:
								
								break;
							case 13:
								
								break;
							case 14:
			
								break;
							case 15:
								
								break;
							case 16:
								
								break;
							case 17:
			
								break;
							case 18:
								
								break;
							case 19:
								
								break;
							case 20:
			
								break;
							case 21:
								
								break;
							case 22:
								
								break;
							case 23:
			
								break;
							case 24:
								
								break;
							case 25:
								
								break;
							case 26:
			
								break;
							case 27:
								
								break;
							case 28:
								
								break;
							case 29:
			
								break;
							case 30:
								
								break;
							case 31:
								
								break;
							case 32:
			
								break;
							case 33:
								
								break;
							case 34:
								
								break;
							case 35:
			
								break;
							case 36:
								
								break;
							case 37:
								
								break;
							case 38:
			
								break;
							case 39:
			
								break;
						}
					break;
				}
            	

                
                
                
                
            } else {
                if (msgFromServer.msgType.equals("join")) {
                    System.out.println("Joined");
                } else if (msgFromServer.msgType.equals("gameInit")) {
                
                
                //AQUI ES DONDE TENGO QUE COGER LOS DATOS DEL CIRCUITO
                System.out.println("Race init");

                //long t_ini = System.currentTimeMillis();
                
            	//LEO EL MENSAJE Y LO CONVIERTO A STRING
                Object info = msgFromServer.data;	
                String s_info = info.toString();
                //System.out.println(s_info);
                
                //SEPARO: pieces, lanes, startingPoint, cars, raceSession
                //Indices
                int pieces = s_info.indexOf("pieces");
                int pieces_lanes = s_info.indexOf("lanes");
                int lanes_startP = s_info.indexOf("startingPoint");
                int startP_cars = s_info.indexOf("cars");
                int raceSession = s_info.indexOf("raceSession");
                //Strings
                String piezas = s_info.substring(pieces, pieces_lanes-2);
                String carriles = s_info.substring(pieces_lanes, lanes_startP-2);
                String PInicio = s_info.substring(lanes_startP, startP_cars-2);
                String coches = s_info.substring(startP_cars, raceSession-2);
                String carrera = s_info.substring(raceSession, s_info.length()-2);       
                
                //PARA SEPARAR CADA COMPONENTE INDIVIDUALMENTE EN ARRAYS DE STRING
                String [] Aux = null;
                int ini, fin;
                
                //SEPARO LAS PIEZAS
                Aux = piezas.split("}");
                PIEZAS = new String[Aux.length-1];
                for (int i = 0; i < Aux.length-1; i++) {
                	ini = Aux[i].contains("length") ? Aux[i].indexOf("length") : Aux[i].indexOf("radius");
                	PIEZAS[i] = Aux[i].substring(ini);
                	//System.out.println("Pieza "+i+": "+PIEZAS[i]);
				}
                
                //SEPARO LOS CARRILES
                Aux = carriles.split("}");
                CARRILES = new String[Aux.length-1];
                for (int i = 0; i < Aux.length-1; i++) {
                	ini = Aux[i].indexOf("distanceFromCenter");
                	CARRILES[i] = Aux[i].substring(ini);
                	//System.out.println(CARRILES[i]);
				}
                
                //SEPARO EL PUNTO DE INICIO
                PINICIO = new String[1];
                ini = PInicio.indexOf("position");
                PINICIO[0] = PInicio.substring(ini, PInicio.length()-2);
                //System.out.println(PINICIO[0]);

                //SEPARO LOS COCHES
                Aux = coches.split("id=");
                COCHES = new String[Aux.length-1];
                for (int i = 1; i < Aux.length; i++) {
                	fin = Aux[i].indexOf("dimensions");
                	String AUX = "id=" + Aux[i].substring(0, fin);
                	String AUX2 = Aux[i].substring(fin, Aux[i].length());
                	int fin2 = AUX2.indexOf("}");
                	AUX = AUX + AUX2.substring(0, fin2+1);
					COCHES[i-1] = AUX;
					//System.out.println(COCHES[i-1]);
				}
                
                //SEPARO LA CARRERA
                CARRERA = new String[1];
                ini = carrera.indexOf("laps");
                CARRERA[0] = carrera.substring(ini, carrera.length()-1);
                //System.out.println(CARRERA[0]);
                /*
                //SEPARO LOS TRAMOS
                int pos = 0; //Posicion donde inserto en TRAMOS
                TRAMOS = new String[PIEZAS.length];
                //recta -> true, curva -> false
                boolean [] tramos = new boolean[PIEZAS.length];
                for (int i = 0; i < PIEZAS.length; i++) {
					tramos[i] = PIEZAS[i].contains("length")? true : false;
				}
                
                int iniTramo = 0, finTramo;
                for (int i = 0; i < tramos.length-1; i++) {
					if(tramos[i] != tramos[i+1] || (i+1)==tramos.length-1){
						finTramo = i;
						double dist = 0;
						double radio = 0;
						double angulo = 0;
						if(tramos[i]){
							for (int j = iniTramo; j <= finTramo; j++) {
								dist += Double.parseDouble(PIEZAS[j].substring(PIEZAS[j].indexOf("=")+1, PIEZAS[j].indexOf(".")+2));
							}
							int numP = finTramo-iniTramo+1;
							if((i+1)==tramos.length-1){
								numP++;
								dist += Double.parseDouble(PIEZAS[finTramo+1].substring(PIEZAS[finTramo+1].indexOf("=")+1, PIEZAS[finTramo+1].indexOf(".")+2));
							}
							TRAMOS[pos] = "NumPiezas="+numP+", Distancia="+dist;
						}
						else{
							radio = Double.parseDouble(PIEZAS[iniTramo].substring(PIEZAS[iniTramo].indexOf("=")+1, PIEZAS[iniTramo].indexOf(".")+2));
		                	angulo = Double.parseDouble(PIEZAS[iniTramo].substring(PIEZAS[iniTramo].indexOf("angle")+6, PIEZAS[iniTramo].indexOf("angle")+10));
		                	int numP = finTramo-iniTramo+1;
		                	TRAMOS[pos] = "NumPiezas="+numP+", Radio="+radio+", Angulo="+angulo;
		                	for (int j = iniTramo+1; j <= finTramo; j++) {
		                		double auxR = Double.parseDouble(PIEZAS[j].substring(PIEZAS[j].indexOf("=")+1, PIEZAS[j].indexOf(".")+2));
								double auxA = Double.parseDouble(PIEZAS[j].substring(PIEZAS[j].indexOf("angle")+6, PIEZAS[j].indexOf("angle")+10));
		                		if(radio != auxR || angulo != auxA){
									pos++;
									radio = auxR;
									angulo = auxA;
				                	TRAMOS[pos] = "NumPiezas="+ -1 +", Radio="+radio+", Angulo="+angulo;
								}
							}
						}
						dist = 0;
						iniTramo = finTramo+1;
						pos++;
					}
				}
				*/
                /*
                for (int i = 0; i < TRAMOS.length; i++) {
					System.out.println(TRAMOS[i]);
				}
                */

                //long t_fin = System.currentTimeMillis();
                
                //System.out.println("Tiempo de ejecucion de gameInit: " + (t_fin-t_ini));
                
                
                
                } else if (msgFromServer.msgType.equals("gameEnd")) {
                    System.out.println("Race end");
                } else if (msgFromServer.msgType.equals("gameStart")) {
                    System.out.println("Race start");
                }
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}


class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}



class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Turbo extends SendMsg {
    private String value;

    public Turbo(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

